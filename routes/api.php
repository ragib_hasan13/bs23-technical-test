<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/update-rider-location', [\App\Http\Controllers\RiderLocationsController::class, 'updateRiderLocation'])->name('rider-location.update-rider-location');
Route::post('/get-nearby-rider-location', [\App\Http\Controllers\RiderLocationsController::class, 'getNearbyRiderLocation'])->name('rider-location.get-rider-location');

