<?php

namespace App\Http\Controllers;

use App\Models\Restaurant;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class RestaurantsController extends Controller
{
    public function create()
    {
        return view('restaurants.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'address' => 'required',
            'lat' => 'required',
            'long' => 'required',
        ]);

        try {
            $restaurant = new Restaurant();

            $restaurant->restaurant_name = $request->name;
            $restaurant->address = $request->address;
            $restaurant->lat = $request->lat;
            $restaurant->long = $request->long;

            $restaurant->save();

            return redirect()->route('restaurant.create');

        } catch (\Exception $e) {
            Log::debug($e->getMessage());
        }
    }
}
