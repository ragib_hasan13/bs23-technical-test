<?php

namespace App\Http\Controllers;

use App\Models\Rider;
use Illuminate\Http\Request;

class RidersController extends Controller
{
    public function create()
    {
        return view('riders.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'phone' => 'required',
        ]);

        try {
            $rider = new Rider();

            $rider->rider_name = $request->name;
            $rider->rider_phone = $request->phone;

            $rider->save();

            return redirect()->route('rider.create');

        } catch (\Exception $e) {
            Log::debug($e->getMessage());
        }
    }
}
