<?php

namespace App\Http\Controllers;

use App\Models\Restaurant;
use App\Models\Rider;
use App\Models\RiderLocation;
use Illuminate\Http\Request;

class RiderLocationsController extends Controller
{
    public function updateRiderLocation(Request $request)
    {
        $validated = $request->validate([
            'rider_id' => 'required',
            'service_name' => 'required',
            'lat' => 'required',
            'long' => 'required',
        ]);

        if (!$validated) {
            return response()->json([
                'message' => 'Validation Error'
            ], 200);
        }

        $riderLoc = new RiderLocation();

        $riderLoc->rider_id = $request->rider_id;
        $riderLoc->service_name = $request->service_name;
        $riderLoc->lat = $request->lat;
        $riderLoc->long = $request->long;
        $riderLoc->capture_time = $request->capture_time;

        $riderLoc->save();

        return response()->json([
            'message' => 'Updated rider location'
        ]);
    }

    /**
     * @param Request $request
     * @return mixed
     *
     */
    public function getNearbyRiderLocation(Request $request)
    {
        $restarant = Restaurant::findorFail($request->restaurant_id);

        $riders = Rider::with('rider_locations')->get();

        $nearestRider = '';
        $nearestRiderDistance = 0;
        $riderLocs = [];

        $res_lat = $restarant->lat;
        $res_long = $restarant->long;
        foreach ($riders as $rider) {
            $theta = $res_long - $rider->rider_locations[0]->long;
            $dist = sin(deg2rad($res_lat)) * sin(deg2rad($rider->rider_locations[0]->lat)) +  cos(deg2rad($res_lat)) * cos(deg2rad($rider->rider_locations[0]->lat)) * cos(deg2rad($theta));
            $dist = acos($dist);
            $dist = rad2deg($dist);
            $miles = $dist * 60 * 1.1515;
            $kms = ($miles * 1.609344);

            if ($nearestRiderDistance == 0) {
                $nearestRiderDistance = $kms;
            }

            $riderLocs[$rider->rider_name] = number_format((float)$kms, 2, '.', '') . ' km';
        }

        return response()->json($riderLocs);
    }
}
