<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Rider extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function rider_locations()
    {
        return $this->hasMany(RiderLocation::class);
    }
}
