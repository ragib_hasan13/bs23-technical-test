<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RiderLocation extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function riders()
    {
        return $this->belongsTo(Rider::class);
    }

}
